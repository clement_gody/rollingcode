function affiche(p, m) {
    for (var i = p; i <= m; i++) {
        var a = document.getElementById(i);
        a.style.display = 'block'
    }
}

function cache(o, l) {
    for (var j = o; j <= l; j++) {
        var b = document.getElementById(j);
        if (b.style.display == 'block') {
            b.style.display = 'none';
        }
    }
}

var json = [
    {
        type: "radio",
        id: "1",
        name: "q1",
        title: "1. In which sector of activity is your company active? (linked with your APE code)",
        isRequired: true,
        choices: [
                "Industry",
                "Business",
                "Information and communication",
                "Other services activity",
                "All other activities (financial, agricultural activities ....)",
                "Construction",
                "Public sector",
                "Specialized, Scientific and Technical or Administrative and Support Services Activities",
                "Real estate",
            ]
        },{
        type: "radio",
        id: "2",
        name: "q2",
        title: "2. What is the number of employees in your company on 18/12/31 ?",
        isRequired: true,
        choices: [
                "0",
                "1 to 2",
                "3 to 9",
                "10 to 49",
                "50 to 249",
                "250 to 4999",
                "More than 5000",
            ]
        },{
        type: "radio",
        id: "3",
        name: "q3",
        title: "3. What is the turnover of your company in the last fiscal year? (or annual budget for Public sector)",
        isRequired: true,
        choices: [
                "0 to 100K $",
                "100 to 500 K $",
                "500 to 2 M $",
                "2 to 10 M $",
                "10 to 50 M $",
                "More than 50 M $",
            ]
        },  {
        type: "radio",
        id: "4",
        name: "q4",
        title: "4. Do you develop digital services for internal or external use (sales to customers)?",
        isRequired: true,
        choices: [
                "Yes",
                "No",
            ]
        },
    {
        type: "radio",
        id: "5",
        name: "q5",
        title: "5. What is the number of users of your digital services?",
        isRequired: true,
        choices: [
                "0 to 100K $",
                "100 to 500 K $",
                "500 to 2 M $",
                "2 to 10 M $",
                "10 to 50 M $",
                "More than 50 M $",
            ]
        
        }, {
        type: "radio",
        id: "6",
        name: "q6",
        title: "6. Do you apply the rules and best practices for digital accessibility?",
        isRequired: true,
        choices: [
                "Yes",
                "No",
            ]
        }, {
        type: "radio",
        id: "7",
        name: "q7",
        title: "7. Have you optimized the states and printouts in your application tools (reduced number of pages when printing, ink consumption ...)",
        isRequired: true,
        choices: [
                "Yes",
                "No",
            ]
        }, {
        type: "radio",
        id: "8",
        name: "q8",
        title: "8. Do you integrate the principles of the ecodesign of digital services?",
        isRequired: true,
        choices: [
                "Yes",
                "No",
            ]
        }, {
        type: "radio",
        id: "9",
        name: "q9",
        title: "9. Do you use a modular application architecture?",
        isRequired: true,
        choices: [
                "Yes",
                "No",
            ]
        }, {
        type: "radio",
        id: "10",
        name: "q10",
        title: "10. Do you do a design review at the end of your application's development?",
        isRequired: true,
        choices: [
                "Yes",
                "No",
            ]
        }, {
        type: "radio",
        id: "11",
        name: "q11",
        title: "11. What is the overall storage volume of your corporate data (centralized on external hard drives, centralized server, NAS, SAN ...) in Terabytes (TB) useful?",
        isRequired: true,
        choices: [
                "I don't know",
                "I do not want to answer",
		"x TB"
            ]
        }, {
        type: "radio",
        id: "12",
        name: "q12",
        title: "12. Do you have a server or do you only work with one or more workstations?",
        isRequired: true,
        choices: [
                "We work with workstation (s), without centralized physical server",
                "We have (at least) a centralized physical server",
            ]
        }, {
        type: "radio",
        id: "13",
        name: "q13",
        title: "13. Do you have a dedicated room, simple room or cupboard with bay dedicated to your IT infrastructure?",
        isRequired: true,
        choices: [
                "A closet or a room without any specific system",
                "A dedicated room",
            ]
        }, {
        type: "radio",
        id: "14",
        name: "q14",
        title: "14. Is your computer room in house or at a host?",
        isRequired: true,
        choices: [
                "Internal",
                "Host Member of the European Code of Conduct for Datacenters",
		"Non-adhering Host of the European Code of Conduct for Data Centers",
            ]
        }, {
        type: "radio",
        id: "15",
        name: "q15",
        title: "15. What is the total area of your computer rooms (excluding technical infrastructure *)? (in m2)",
        isRequired: true,
        choices: [
                "I don't know",
                "I do not want to answer",
		"x m2"
            ]
        }, {
        type: "radio",
        id: "16",
        name: "q16",
        title: "16. Do you know the PUE * of your Data Center? (*PUE : Power Usage Effectiveness)",
        isRequired: true,
        choices: [
                "Less than 1,6 ",
                "Between 1,6 and 2,1 ",
		"More than 2,1 ",
		"I don't know",
            ]
        }, {
        type: "radio",
        id: "17",
        name: "q17",
        title: "17. What is the rate of charge or energy use of your computer rooms?(Rate = Electrical power absorbed by your IT equipment, divided by room capacity in kW, then multiplied by 100 (used energy / available energy))",
        isRequired: true,
        choices: [
            "100% - 90%",
            "90% - 60% ",
            "Less than 60% ",
            "I don't know",
            ]
        },
    {
        type: "radio",
        id: "18",
        name: "q18",
        title: "18.Have you led or are you planning actions to optimize your infrastructure? Especially ?",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 },
    {
        type: "radio",
        id: "19",
        name: "q19",
        title: "19.The purchase of non-IT equipment from IT rooms (air conditioning, air treatment, inverters, etc.) according to energy efficiency criteria",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 }, {
        type: "radio",
        id: "20",
        name: "q20",
        title: "20. Implementing the good practices of the \"European Code of Conduct for DataCenter\"?",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 },
    {
        type: "radio",
        id: "21",
        name: "q21",
        title: "21. Data center PUE tracking",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 },
    {
        type: "radio",
        id: "22",
        name: "q22",
        title: "22.Regular monitoring of environmental indicators of computer rooms",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 },
    {
        type: "radio",
        id: "23",
        name: "q23",
        title: "23.Environmental impact analysis of the datacenter in life cycle approach",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 },
    {
        type: "radio",
        id: "24",
        name: "q24",
        title: "24.ptimizing the architecture and layout of rooms",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 },
    {
        type: "radio",
        id: "25",
        name: "q25",
        title: "25.The urbanization of halls in hot / cold aisles",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 },
    {
        type: "radio",
        id: "26",
        name: "q26",
        title: "26.Containment of air flows (corridors)",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 },
    {
        type: "radio",
        id: "27",
        name: "q27",
        title: "27.The use of natural cooling sources (freecooling)",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 },
    {
        type: "radio",
        id: "28",
        name: "q28",
        title: "28.Implementation of a heat recovery system for computer rooms (heating)",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 },
    {
        type: "radio",
        id: "29",
        name: "q29",
        title: "29.The set temperature in the cold corridor remains higher than 24 °",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 },
    {
        type: "radio",
        id: "30",
        name: "q30",
        title: "30.The choice of a modular datacenter architecture",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 },
    {
        type: "radio",
        id: "31",
        name: "q31",
        title: "31.Have you led or are you planning actions to optimize your infrastructure? Especially :",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 },
    {
        type: "radio",
        id: "32",
        name: "q32",
        title: "32. Suspending network equipment",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 },
    {
        type: "radio",
        id: "33",
        name: "q33",
        title: "33.Pooling physical equipment",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 },
    {
        type: "radio",
        id: "34",
        name: "q34",
        title: "34.Uninstalling unnecessary infrastructure",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 },
    {
        type: "radio",
        id: "35",
        name: "q35",
        title: "Traceability of material elements (CMDB)",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 },
    {
        type: "radio",
        id: "36",
        name: "q36",
        title: "36.The correct sizing of the servers in relation to their use",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 },
    {
        type: "radio",
        id: "37",
        name: "q37",
        title: "37.Give priority to ASHRAE 2 compatible equipment",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 },
    {
        type: "radio",
        id: "38",
        name: "q38",
        title: "38.A procedure for provisioning and de-provisioning data-processing equipment in datacenters",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
 }, {
        type: "radio",
        id: "39",
        name: "q39",
        title: "39.Do you know the number of physical servers and virtual servers in your company ?",
        isRequired: true,
        choices: [
                "No",
                "I do not want to answer",
                "Yes",
]

        }, {
        type: "radio",
        id: "40",
        name: "q40",
        title: "40.How many physical servers do you have?",
        isRequired: true,
        choices:[
            "0-5",
            "5-10",
            "10-20",
            "More",
        ]
        },

    {
        type: "radio",
        id: "41",
        name: "q41",
        title: "41.How many virtual servers do you have?",
        isRequired: true,
          choices:[
            "0-5",
            "5-10",
            "10-20",
            "More",
        ]
        },{
        type: "radio", //text sur deux reponse
        id: "42",
        name: "q42",
        title: "42.What will be the evolution of your number of physical servers for 2019? (in% or quantity)",
        isRequired: true,
        choices: [
            "In %>>",
            "In quantity>>",
            "I don't know",
            "I don't want to answer",
            ]
        },

    {
        type: "radio", //text sur deux reponse
        id: "43",
        name: "q43",
        title: "43.What will be the evolution of your number of virtual servers for 2019? (in% or quantity)",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
        },

    {
        type: "radio",
        id: "44",
        name: "q44",
        title: "44.Has your company appointed a Green IT Manager / Digital Manager?",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
        },

    {
        type: "radio",
        id: "45",
        name: "q45",
        title: "45.Do you have a responsible digital strategy broken down into an action plan?",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
        },

    {
        type: "radio",
        id: "46",
        name: "q46",
        title: "46.Is Green IT a topic integrated into your CSR strategy?",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
        },

    {
        type: "radio",
        id: "47",
        name: "q47",
        title: "47.Do you regularly evaluate the environmental impacts of your information system?",
        isRequired: true,
        choices: [
                "Yes partially, including only equipment present in the company",
		"Yes totally, including our internal equipment and services hosted by third parties",
                "No",
                "I don't know",
            ]
        },

    {
        type: "radio",
        id: "48",
        name: "q48",
        title: "48.Do you have a team of competent referees on the topics of Green IT?",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
        },

    {
        type: "radio",
        id: "49",
        name: "q49",
        title: "49.Have you integrated Green IT into your business strategy",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
        },

    {
        type: "radio",
        id: "50",
        name: "q50",
        title: "50.Do you have those equipments in your compagny:",
        isRequired: true,
        choices: [
                "Used",
                "Not used functional",
                "Neither used nor functional",
            ]
        },

    {
        type: "radio",
        id: "51",
        name: "q51",
        title: "51.Fixed stations, workstations",
        isRequired: true,
        choices: [
                "Used",
                "Not used functional",
                "Neither used nor functional",
            ]
        },

    {
        type: "radio",
        id: "52",
        name: "q52",
        title: "52.Laptops, digital tablets",
        isRequired: true,
        choices: [
                "Used",
                "Not used functional",
                "Neither used nor functional",
            ]
        },

    {
        type: "radio",
        id: "53",
        name: "q53",
        title: "53.Small printers (<15kg, potentially used by a household)",
        isRequired: true,
        choices: [
                "Used",
                "Not used functional",
                "Neither used nor functional",
            ]
        },

    {
        type: "radio",
        id: "54",
        name: "q54",
        title: "54.Flat screen monitors",
        isRequired: true,
        choices: [
                "Used",
                "Not used functional",
                "Neither used nor functional",
            ]
        },

    {
        type: "radio",
        id: "55",
        name: "q55",
        title: "55.Other flat screens (TV, projection screen, digital board ...)",
        isRequired: true,
        choices: [
                "Used",
                "Not used functional",
                "Neither used nor functional",
            ]
        },

    {
        type: "radio",
        id: "56",
        name: "q56",
        title: "56.CRT monitors (monitors or other)",
        isRequired: true,
        choices: [
                "Used",
                "Not used functional",
                "Neither used nor functional",
            ]
        },

    {
        type: "radio",
        id: "57",
        name: "q57",
        title: "57.Video projectors",
        isRequired: true,
        choices: [
                "Used",
                "Not used functional",
                "Neither used nor functional",
            ]
        },

    {
        type: "radio",
        id: "58",
        name: "q58",
        title: "58.Mobile phones",
        isRequired: true,
        choices: [
                "Used",
                "Not used functional",
                "Neither used nor functional",
            ]
        },

    {
        type: "radio",
        id: "59",
        name: "q59",
        title: "59.Fixed telephones (standalone not connected to such a standard)",
        isRequired: true,
        choices: [
                "Used",
                "Not used functional",
                "Neither used nor functional",
            ]
        },

    {
        type: "radio",
        id: "60",
        name: "q60",
        title: "60.Digital cameras",
        isRequired: true,
        choices: [
                "Used",
                "Not used functional",
                "Neither used nor functional",
            ]
        },
    {
        type: "radio",
        id: "61",
        name: "q61",
        title: "61.Hard Disk Devices, Storage, Backup",
        isRequired: true,
        choices: [
                "Used",
                "Not used functional",
                "Neither used nor functional",
            ]
        }, {
        type: "radio",
        id: "62",
        name: "q62",
        title: "62.Do you have other devices in your company ?",
        isRequired: true,
        choices: [
                "Used",
                "Not used functionnal",
                "Neither used nor functionnal",
            ]
        },  {
        type: "radio",
        id: "63",
        name: "q63",
        title: "63.Do you have other devices in your company ?",
        isRequired: false,
        choices: [
                "Yes",
                "No",
            ]
        },{
        type: "radio",
        id: "64",
        name: "q64",
        title: "64.Keyboards",
        isRequired: true,
        choices: [
                "Used",
                "Not used functionnal",
                "Neither used nor functionnal",
            ]
        }, {
        type: "radio",
        id: "65",
        name: "q65",
        title: "65.Mouse",
        isRequired: true,
        choices: [
                "Used",
                "Not used functionnal",
                "Neither used nor functionnal",
            ]
        }, {
        type: "radio",
        id: "66",
        name: "q66",
        title: "66.Graphic tablets",
        isRequired: true,
        choices: [
                "Used",
                "Not used functionnal",
                "Neither used nor functionnal",
            ]
        }, {
        type: "radio",
        id: "67",
        name: "q67",
        title: "67.Scanners",
        isRequired: true,
        choices: [
                "Used",
                "Not used functionnal",
                "Neither used nor functionnal",
            ]
        }, {
        type: "radio",
        id: "68",
        name: "q68",
        title: "68.Speakers",
        isRequired: true,
        choices: [
                "Used",
                "Not used functionnal",
                "Neither used nor functionnal",
            ]
        }, {
        type: "radio",
        id: "69",
        name: "q69",
        title: "69.Office automation",
        isRequired: true,
        choices: [
                "Used",
                "Not used functionnal",
                "Neither used nor functionnal",
            ]
        }, {
        type: "radio", //pas de type radio car si la reponse est "yes" il faut spécifier le nombre de kWh
        id: "70",
        name: "q70",
        title: "70.Do you know the consumption of your worksation in kWh per year ?",
        isRequired: true,
        choices: [
                "Yes (please specify how much in kWh / year)",
                "No",
            ]
        }, {
        type: "radio",
        id: "71",
        name: "q71",
        title: "71.Do you track the energy consumption of your compagny activities ?",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
        }, {
        type: "radio", //pas de type radio car si c'est "yes" il faut spécifier combien de %
        id: "72",
        name: "q72",
        title: "72.Do you know the share of IT and IT equipement in your company's total energy consumption ?",
        isRequired: true,
        choices: [
                "Yes (please specify how much in %)",
                "No",
            ]
        }, {
        type: "radio",
        id: "73",
        name: "q73",
        title: "73.Have you set up a power management system ? (automatic shutdown / shutdown of workstations)",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
        }, {
        type: "radio",
        id: "74",
        name: "q74",
        title: "74.Do you use copiers from a repackaging industry (second-hand / second-hand) ?",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
        }, {
        type: "radio",
        id: "75",
        name: "q75",
        title: "75.Do you consolidate individual printers to shared printers",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
        }, {
        type: "radio",
        id: "76",
        name: "q76",
        title: "76.Have you set up an identification system on printers (to trigger printing) ?",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
        }, {
        type: "radio", //pas de type radio car si c'est "x years" il faut spécifier combien d'années
        id: "77",
        name: "q77",
        title: "77.What is the average life of your professional copier / MFP (MFP = Multi Fonction Printer)",
        isRequired: true,
        choices: [
                "I don't know",
                "Do not want to answer",
                "x years (please specify)",
            ]
        }, {
        type: "radio",
        id: "79",
        name: "q79",
        title: "79.Energy saving (Automatic standby)",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
        },{
        type: "radio", //pas de type radio car si c'est "x years" il faut spécifier combien d'années
        id: "78",
        name: "q78",
        title: "78.What is the average life of your professional copier / MFP (MFP = Multi Fonction Printer)",
        isRequired: false,
        choices: [
                "I don't know",
                "Do not want to answer",
                "x years (please specify)",
            ]
        }, {
        type: "radio",
        id: "80",
        name: "q80",
        title: "80.Black and white by default",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
        }, {
        type: "radio",
        id: "81",
        name: "q81",
        title: "81.Default duplex",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
        }, {
        type: "radio",
        id: "82",
        name: "q82",
        title: "82.Default draft mode",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
        }, {
        type: "radio",
        id: "83",
        name: "q83",
        title: "83.What is the number of pages printed / day / employee (A4 equivalent) ?",
        isRequired: true,
        choices: [
                "Less than 10",
                "From 10 to 20",
                "From 20 to 30",
                "More than 30",
                "I don't know",
            ]
        }, {
        type: "radio", // Pas de type radio car pour chaque reponse il doit écrire une réponse
        id: "84",
        name: "q84",
        title: "84.Can you specify the number of cartridges / toners:",
        isRequired: true,
        choices: [
                "Cartridges used a year",
                "Cartridges stored in the average business",
                "Toners used a year",
                "Toners stored in the average business",
            ]
        }, {
        type: "radio",
        name: "q85",
        id: "85",
        title: "85.Do you organize the separate collection of waste cartridges / toners ?",
        isRequired: true,
        choices: [
                "Yes, to a repackaging industry",
                "Yes, mixed paper",
                "No, no separate collection device is planned",
            ]
        }, {
        type: "radio",
        id: "86",
        name: "q86",
        title: "86.Do you prefer the use of recycled paper ?",
        isRequired: true,
        choices: [
                "Yes, our paper is made from virgin paste",
                "Yes, mixed paper",
                "Yes, 100 % recycled",
                "I'm not paying attention",
            ]
        }, {
        type: "radio", //pas de type radio car pour la réponse "yes, other" il doit écrire a coté de la réponse
        id: "87",
        name: "q87",
        title: "87.Do you choose certified paper ?",
        isRequired: true,
        choices: [
                "Yes, FSC",
                "Yes, PEFC",
                "Yes, Blue Angel",
                "Yes, European Label",
                "Yes, other >> comment fields to add",
            ]
        }, {
        type: "radio",
        id: "88",
        name: "q88",
        title: "88.Do you organize the separate collection of waste paper for recycling ?",
        isRequired: true,
        choices: [
                "Yes",
                "No",
                "I don't know",
            ]
        }
    
    ];
